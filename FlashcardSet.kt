package cs.mad.flashcards.entities

// Transform FlashcardSet into entity
@Entity
data class FlashcardSet(
    @PrimaryKey
    val id: Long? = null,
    val title: String,
    @ColumnInfo(title = "flashcardset_title")

) {
    companion object {
        fun getHardcodedFlashcardSets(): List<FlashcardSet> {
            val sets = mutableListOf<FlashcardSet>()
            for (i in 1..10) {
                sets.add(FlashcardSet("Set $i"))
            }
            return sets
        }
    }
}