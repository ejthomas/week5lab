package cs.mad.flashcards.entities

// Transform Flashcard into entity
@Entity
data class Flashcard(
    @PrimaryKey
    val id: Long,
    val question: String,
    val answer: String,
    @ColumnInfo(question = "flashcard_question")
    @ColumnInfo(answer = "flashcard_answer")

) {
    companion object {
        fun getHardcodedFlashcards(): List<Flashcard> {
            val cards = mutableListOf<Flashcard>()
            for (i in 1..10) {
                cards.add(Flashcard("Term $i", "Def $i"))
            }
            return cards
        }
    }
}